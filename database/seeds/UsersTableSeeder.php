<?php

use Illuminate\Database\Seeder;
use App\User ;
use App\Role ;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::truncate() ;
        DB::table('role_user')->truncate() ;

        $adminRole  = Role::where('name' , 'admin')->first() ;
        $editorRole  = Role::where('name' , 'editor')->first()  ;
        $companyRole  = Role::where('name'  , 'company')->first() ;
        $employeeRole  = Role::where('name' , 'employee')->first()  ;

        $admin    = User::create(['name' => 'admin' , 'email' => 'admin@admin.com' , 'password' => Hash::make('password')]) ;
        $editor   = User::create(['name' => 'editor' , 'email' => 'editor@editor.com' , 'password' => Hash::make('password')]) ;
        $company  = User::create(['name' => 'company' , 'email' => 'company@company.com' , 'password' => Hash::make('password')]) ;
        $employee = User::create(['name' => 'employee' , 'email' => 'employee@employee.com' , 'password' => Hash::make('password')]) ;

        $admin->roles()->attach($adminRole);
        $editor->roles()->attach($editorRole);
        $company->roles()->attach($companyRole) ;
        $employee->roles()->attach($employeeRole) ;
    }
}
