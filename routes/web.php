<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('lang/{locale}', 'HomeController@lang');
Route::get('/companies/pdfDownload', 'CompanyController@pdfDownload');
Route::resource('employees', 'EmployeeController');
Route::resource('companies', 'CompanyController');
Route::get('/companies' ,'CompanyController@index')->name('companies') ;
Route::get('/employees' , 'EmployeeController@index')->name('employees') ;

//Route::group(['prefix' => 'dashboard'], function() {
//    Route::view('/', 'dashboard/dashboard');
//    Route::get('companies/create', 'CompanyController@create')->name('companies.create');
//    Route::get('employees/create/{id}', 'EmployeeController@create');
//    Route::resource('employees', 'EmployeeController')->except('create');
//
//});