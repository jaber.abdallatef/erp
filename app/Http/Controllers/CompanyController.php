<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;
use App\Models\Company ;
use Symfony\Component\Console\Input\Input;
use Illuminate\Support\Facades\Validator;
use Image ;
use Illuminate\Support\Facades\Storage ;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Redirect ;
use Barryvdh\DomPDF\Facade as PDF;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $companies = Company::paginate(30);
        return view('dashboard/companyIndex')->with('companies', $companies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('dashboard/companyForm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $rules = array(
            'name'       => 'required|unique:companies',
            'address'      => 'required',
            'website' => ['regex:/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/'],
            'logo' =>  'required|file|max:1024',
        );
        $validator      =   Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return back()->withErrors($validator->errors());
        }

        if($image  =   $request->file('logo')) {
            $imagePath = $request->file('logo') ;
            $filename  =base64_encode(time().rand()).'.'.$imagePath->getClientOriginalExtension();
            $image = Image::make($imagePath->getRealPath())->resize(100,100);
            $image->stream();
            Storage::disk('local')->put($filename,$image, 'public');
        }
         $company = new Company() ;
         $company->name  = $request->input('name') ;
         $company->address = $request->input('address') ;
         $company->website    = $request->input('website') ;
         $company->logo = $filename  ;
         $company->save() ;
          /// send  email to company's  mail  address


        Mail::send('emails.reminder', ['user' => $company], function ($m) use ($company) {
            $m->from('info@erp.com', 'ERP Application');

            $m->to($company->email, $company->name)->subject('Congratulations!');
        });

         return Redirect::to('companies')->withSuccess('Company Added');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $company = Company::find($id);
        return view('dashboard/companyView')->with('company', $company);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $company = Company::find($id) ;
        return view('dashboard/companyForm')->with('company' , $company) ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $company = Company::find($id);
        $rules = array(
            'name'       => 'required|unique:companies',
            'address'      => 'required',
            'website' => ['regex:/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/'],
            'logo' =>  'file|max:1024',
        );
        $validator      =   Validator::make($request->all(),$rules);
        if($validator->fails()) {
            return back()->withErrors($validator->errors());
        }
       else
       {
           if($image  =   $request->file('logo')) {
               $imagePath = $request->file('logo') ;
               $filename  =base64_encode(time().rand()).'.'.$imagePath->getClientOriginalExtension();
               $image = Image::make($imagePath->getRealPath())->resize(100,100);

               $image->stream();
               Storage::disk('local')->put($filename,$image, 'public');
               $company->logo = $filename  ;
           }

           $company->name  = $request->input('name') ;
           $company->address = $request->input('address') ;
           $company->website    = $request->input('website') ;

           $company->save() ;

           return Redirect::to('companies')->withSuccess('Company Is Updated');
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // delete
        $company= Company::find($id);
        $company->delete();

        // redirect

        return Redirect::to('companies')->withSuccess('Company Is Deleted!');
    }

    public function pdfDownload(){


         $companies = Company::all() ;
         $data = [] ;
          foreach ($companies as $company)
          {

               $item = [
                    'companyName' => $company->name  ,
                     'numOfEmps' => count($company->employees()->get())
               ] ;
               array_push($data , $item) ;
          }


        $pdf = PDF::loadView('dashboard/companyReport', array('data' => $data));

        return $pdf->download('companyReport.pdf');
    }
}
