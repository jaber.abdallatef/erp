<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEmployee;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Http\Request;
use  Intervention\Image\Facades\Image ;
use Illuminate\Support\Facades\Storage ;
use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\Redirect ;
class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $employees = Employee::paginate(30);
        return view('dashboard/employeeIndex')->with('employees', $employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $companies  = Company::all() ;
        return view('dashboard/employeeForm')->with('companies',$companies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEmployee $request)
    {
        //

        if(!$request->validated()) {
            return back()->withErrors();
        }

        if($image  =   $request->file('profile_picture')) {
            $imagePath = $request->file('profile_picture') ;
            $filename  =base64_encode(time().rand()).'.'.$imagePath->getClientOriginalExtension();
            $image = Image::make($imagePath->getRealPath())->resize(100,100);

            $image->stream();
            Storage::disk('local')->put($filename,$image, 'public');
        }
        $employee = new Employee() ;
        $employee->first_name  = $request->input('first_name') ;
        $employee->last_name = $request->input('last_name') ;
        $employee->email    = $request->input('email') ;
        $employee->date_of_birth = $request->input('date_of_birth') ;
        $employee->phone  = $request->input('phone') ;
        $employee->profile_picture = $filename  ;
        $employee->company_id = $request->input('company_id')  ;
        $employee->save() ;

        return Redirect::to('employees')->withSuccess('Employee Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $employee = Employee::find($id);
        return view('dashboard/employeeView')->with('employee', $employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $employee= Employee::find($id) ;
        $companies  = Company::all() ;
        return view('dashboard/employeeForm', ['companies' => $companies , 'employee' => $employee]) ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreEmployee $request, $id)
    {

        if(!$request->validated()) {
            return back()->withErrors();
        }
        $employee = Employee::find($id);
        if($image  =   $request->file('profile_picture')) {
            $imagePath = $request->file('profile_picture') ;
            $filename  =base64_encode(time().rand()).'.'.$imagePath->getClientOriginalExtension();
            $image = Image::make($imagePath->getRealPath())->resize(100,100);

            $image->stream();
            Storage::disk('local')->put($filename,$image, 'public');
            $employee->profile_picture = $filename  ;
        }

        $employee->first_name  = $request->input('first_name') ;
        $employee->last_name = $request->input('last_name') ;
        $employee->email    = $request->input('email') ;
        $employee->date_of_birth = $request->input('date_of_birth') ;
        $employee->phone  = $request->input('phone') ;
        $employee->company_id  = $request->input('company_id') ;
        $employee->save() ;

        return Redirect::to('employees')->withSuccess('Employee Updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $employee= Employee::find($id);
        $employee->delete();

        // redirect

        return Redirect::to('employees')->withSuccess('Employee Deleted');
    }
}
