<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use  Illuminate\Support\Facades\Auth ;
class StoreEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
         $user  = Auth::user() ;

           if($user->checkRole('admin'))
           {
                return true ;
           }

      return false  ;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules= [
            'first_name' => 'required|max:40',
            'last_name'  => 'required',
            'email' => 'email:rfc,dns',
            'phone' => 'phone:auto',
            'date_of_birth' =>  'required',
            'profile_picture' =>  'required|file|max:1024',
            'company_id' => 'required'
        ] ;
        if($this->has('have_phone')){
            $rules['phone']  = 'required|phone:auto' ;
        }

        if($this->method() == 'PUT')
        {
            $rules['profile_picture']  = 'file|max:1024' ;
        }
        return  $rules ;
    }
}
