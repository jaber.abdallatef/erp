<?php
 use Illuminate\Routing\Route ;
 $request = request()->route()->getAction() ;
 $actionName  = $request['as'] ;

?>
@extends('layouts.app')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="col-6">
                <div class="card uper">
                    <div class="card-header">
                       {{($actionName=='companies.create')?trans('messages.create_new_company'):trans('messages.update_company')}}
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                            <?php if($actionName =='companies.create'): ?>
                            {{ Form::open(array('url' =>'companies' , 'files'=> true)) }}
                            @csrf
                            <?php  else: ?>
                            {{ Form::open(array('url' =>'companies/'.$company->id , 'files'=> true)) }}
                            @csrf
                            @method('PUT')
                            <?php  endif; ?>
                        <div class="form-group">
                            <label for="name">{{trans('messages.name')}}:</label>
                            <input type="text" class="form-control" name="name" id="name"
                                   value="{{($actionName == 'companies.create')?'':$company->name}}" />
                        </div>
                        <div class="form-group">
                            <label for="address">{{trans('messages.address')}}:</label>
                            <input type="text" class="form-control" name="address" value="{{($actionName == 'companies.create')?'':$company->address}}" id="address" />
                        </div>
                        <div class="form-group">
                            <label for="website">{{trans('messages.website')}}:</label>
                            <input type="url" class="form-control" name="website" id="website" value="{{($actionName == 'companies.create')?'':$company->website}}" />
                        </div>
                        <div class="form-group">
                            <label for="logo">{{trans('messages.logo')}}:</label>
                            <input type="file" class="form-control" name="logo" id="logo" />
                        </div>
                        <button type="submit" class="btn btn-primary">{{trans('messages.submit')}}</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card uper">
                    <div class="card-body">
                        <?php
                        if($actionName == 'companies.edit') :?>
                        <img style="width:100px" src="{{url('storage/'.$company->logo)}} ">
                        <?php endif ; ?>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection