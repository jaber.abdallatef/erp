<h1> Company Report</h1>

<table>
    <thead>
    <tr>
        <th>{{trans('messages.company')}}</th>
        <th>{{trans('messages.count_of_employees')}}</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data ?? '' as $row)
        <tr>
            <td>{{ $row['companyName'] }}</td>
            <td>{{ $row['numOfEmps']  }}</td>
        </tr>
    @endforeach
    </tbody>
</table>