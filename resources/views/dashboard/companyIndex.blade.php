@extends('layouts.app')

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success">
            {{Session::get('success')}}
        </div>
    @endif
    <div class="container">
        <div class="form-group">
            <a href="{{ route('companies.create') }}" type="button" class="btn btn-success">{{trans('messages.create_new_company')}}</a>
            <a href="{{ URL::to('/companies/pdfDownload') }}">{{trans('messages.export_report')}}</a>
        </div>

            <table class="table table-bordered" id="laravel">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>{{trans('messages.name')}}</th>
                    <th>{{trans('messages.address')}}</th>
                    <th>{{trans('messages.website')}}</th>
                    <th>{{trans('messages.logo')}}</th>
                    <th>{{trans('messages.actions')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($companies as $company)
                    <tr>
                        <td>{{ $company->id }}</td>
                        <td>{{ $company->name }}</td>
                        <td>{{ $company->address }}</td>
                        <td>{{$company->website}}</td>
                        <td><img src="{{url('storage/'.$company->logo)}} "></td>
                        <td class="actions-td">
                            <a class="btn btn-small btn-primary" href="{{ route('companies.edit', $company->id) }}">
                                {{trans('messages.edit')}}
                            </a>
                            <a class="btn btn-small btn-warning" href="{{ route('companies.show', $company->id) }}">
                                {{trans('messages.show')}}
                            </a>
                            {{ Form::open(array('url' => 'companies/' . $company->id, 'style' => 'display:inline')) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            {{ Form::submit(trans('messages.delete'), array('class' => 'btn btn-danger')) }}
                            {{ Form::close() }}
                        </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $companies->links() !!}
    </div>
@endsection
