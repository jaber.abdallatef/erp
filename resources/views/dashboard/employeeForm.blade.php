<?php
$request = request()->route()->getAction();
$actionName = $request['as'];
?>
@extends('layouts.app')

@section('content')
    <style>
        .uper {
            margin-top: 40px;
        }
    </style>
    <div class="container">
        <?php if($actionName =='employees.create'): ?>
        {{ Form::open(array('url' =>'employees' , 'files'=> true)) }}
            @csrf
        <?php  else: ?>
            {{ Form::open(array('url' =>'employees/'.$employee->id , 'files'=> true)) }}
            @csrf
            @method('PUT')
         <?php  endif; ?>
        <div class="row">
            <div class="col-6">
                <div class="card uper">
                    <div class="card-header">
                        {{($actionName=='employees.create')?trans('messages.create_new_employee'):trans('messages.update_employee')}}
                    </div>
                    <div class="card-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br/>
                        @endif

                        <div class="form-group">
                            <label for="first_name">{{trans('messages.first_name')}}:</label>
                            <input type="text" class="form-control" name="first_name" id="first_name"
                                   value="{{($actionName == 'employees.create')?'':$employee->first_name}}"/>
                        </div>
                        <div class="form-group">
                            <label for="address">{{trans('messages.last_name')}}:</label>
                            <input type="text" class="form-control" name="last_name"
                                   value="{{($actionName == 'employees.create')?'':$employee->last_name}}"
                                   id="last_name"/>
                        </div>
                        <div class="form-group">
                            <label for="email">{{trans('messages.email')}}:</label>
                            <input type="email" class="form-control"
                                   name="email" value="{{($actionName == 'employees.create')?'':$employee->email}}"
                                   id="email"/>
                        </div>
                        <div class="form-group">
                            <label for="company_id">{{trans('messages.company')}}:</label>
                            <select class="form-control"
                                    value="{{($actionName == 'employees.create')?'':$employee->company_id}}"
                                    name="company_id" id="company_id">
                                <?php foreach ($companies as $company):  ?>
                                <option value="<?=$company->id?>"><?php echo $company->name; ?></option>
                                <?php  endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="website">{{trans('messages.phone')}}:</label>
                            <input type="tel" class="form-control" name="phone" id="phone" value="{{($actionName ==
                            'employees.create')?'':$employee->phone}}"/>
                        </div>

                        <button type="submit" class="btn btn-primary">{{trans('submit')}}</button>

                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card uper">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="address">{{trans('messages.date_of_birth')}}:</label>
                            <input type="date" class="form-control date_picker" name="date_of_birth"
                                   value="{{($actionName == 'employees.create')?'':$employee->date_of_birth}}"
                                   id="date_of_birth"/>
                        </div>
                        <div class="form-group">
                            <label for="logo">{{trans('messages.profile_picture')}}:</label>
                            <input type="file" class="form-control" name="profile_picture" id="profile_picture"/>
                        </div>
                        <?php
                        if($actionName == 'employees.edit') :?>
                        <img style="width:100px" src="{{url('storage/'.$employee->profile_picture)}} ">
                        <?php endif ; ?>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection