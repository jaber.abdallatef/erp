<?php


?>

@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3>{{$company->name}}</h3>
                    </div>
                    <div class="card-body">
                        <img src="{{url('storage/'.$company->logo)}} ">
                         <p>{{$company->address}}</p>
                          <p><a href="{{$company->website}}">Website</a></p>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
