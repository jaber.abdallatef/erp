@extends('layouts.app')

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success">
            {{Session::get('success')}}
        </div>
    @endif
    <div class="container">
        <div class="form-group">
            <a href="{{ route('employees.create') }}" type="button" class="btn btn-success">{{trans('messages.create_new_employee')}}</a>
        </div>

        <table class="table table-bordered" id="laravel">
            <thead>
            <tr>
                <th>Id</th>
                <th>{{trans('messages.first_name')}}</th>
                <th>{{trans('messages.last_name')}}</th>
                <th>{{trans('messages.email')}}</th>
                <th>{{trans('messages.phone')}}</th>
                <th>{{trans('messages.date_of_birth')}}</th>
                <th>{{trans('messages.actions')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($employees as $employee)
                <tr>
                    <td>{{ $employee->id }}</td>
                    <td>{{ $employee->first_name }}</td>
                    <td>{{ $employee->last_name }}</td>
                    <td>{{$employee->email}}</td>
                    <td>{{$employee->phone}}</td>
                    <td>{{$employee->date_of_birth}}</td>
                    <td class="actions-td">
                        <a class="btn btn-small btn-primary" href="{{ route('employees.edit', $employee->id) }}">
                            {{trans('messages.edit')}}
                        </a>
                        <a class="btn btn-small btn-warning" href="{{ route('employees.show', $employee->id) }}">
                            {{trans('messages.show')}}
                        </a>
                        {{ Form::open(array('url' => 'employees/' . $employee->id, 'style' => 'display:inline')) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::submit(trans('messages.delete'), array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $employees->links() !!}
    </div>
@endsection
