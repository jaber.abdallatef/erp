<?php


?>
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h3>{{$employee->first_name.' '.$employee->last_name}}</h3>
                    </div>
                    <div class="card-body">
                        <img src="{{url('storage/'.$employee->profile_picture)}} ">
                         <p>{{$employee->phone}}</p>
                        <p><a mailto="{{$company->email}}">{{$employee->email}}</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
