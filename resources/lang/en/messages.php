<?php
/**
 * Created by PhpStorm.
 * User: Inspiron
 * Date: 1/3/2020
 * Time: 6:36 AM
 */

return [
    'welcome' => 'Welcome To ERP System',

    'home' => 'Home',
    'companies' => 'Companies',
    'employees' => 'Employees',

    'you_are_logged_on' => 'You are logged in!',
    'dashboard' => 'Dashboard',

    'create_new_company' => 'Create New Company',
    'export_report' => 'Export Report',
    'name' => 'Name',
    'address' => 'Address',
    'logo' => 'Logo',
    'actions' => 'Actions',
    'submit' => 'Submit',
    'edit' => 'Edit',
    'show' => 'Show',
    'delete' => 'Delete',
    'create_new_employee' => 'Create New Employee',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'email' => 'Email',
    'phone' => 'Phone',
    'date_of_birth' => 'Date Of Birth',
    'count_of_employees' => 'Count Of Employees',
    'update_company' => 'Update Company',
    'update_employee' => 'Update Employee',
    'profile_picture' => 'Profile Picture',
    'website' => 'Website',
    'company' => 'Company'


];