<?php
/**
 * Created by PhpStorm.
 * User: Inspiron
 * Date: 1/3/2020
 * Time: 6:38 AM
 */

return [
    'welcome' => 'اهلا بك في نظام ERP',
    'home' => 'الرئيسية',
    'companies' => 'الشركات',
    'employees' => 'الموظفين',

    'you_are_logged_on' => 'لقد تم تسجيل الدخول بنجاح',
    'dashboard' => 'لوحة التحكم',

    'create_new_company' => 'انشاء شركة جديدة',
    'export_report' => 'استخراج تقرير',
    'name' => 'الاسم',
    'address' => 'العنوان',
    'logo' => 'االشعار',
    'actions' => 'اوامر',
    'submit' => 'تنفيذ',
    'edit' => 'تعديل',
    'show' => 'اظهار',
    'delete' => 'حذف',
    'create_new_employee' => 'انشاء موظف جديد',
    'first_name' => 'الاسم الاول',
    'last_name' => 'الاسم الثاني',
    'email' => 'البريد الالكتروني',
    'phone' => 'هاتف',
    'date_of_birth' => 'تاريخ الميلاد',
    'company' => 'الشركة',
    'website' => 'الموقع الالكتروني',
    'count_of_employees' => 'عدد الموظفين',
    'update_company' => 'تعديل شركة',
    'update_employee' => 'تعديل الموظف',
    'profile_picture' => 'الصورة الشخصية'

];